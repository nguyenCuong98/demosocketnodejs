const Scheme = require("../model/index");
exports.get = async (req, res) => {
  try {
    let data = await Scheme.find({});
    res.send({ data });
  } catch (error) {
    res.send({ error });
  }
};
exports.add = async (req, res) => {
  try {
    let limit = parseInt(req.query.limit);
    let name = req.body.name;
    let textSearch = req.body.textSearch;
    let data = await Scheme.create({ name: name });
    let dataAdd;
    if (textSearch) {
      dataAdd = await Scheme.countDocuments({
        name: { $regex: textSearch, $options: "i" },
      });
    } else {
      dataAdd = await Scheme.countDocuments();
    }
    let totalPage = Math.ceil(dataAdd / limit);
    res.send({ data, totalPage });
  } catch (error) {
    res.send(error);
  }
};
exports.addCategory = async (req, res) => {
  try {
   let  maCp = req.body.maCp;
   let  thiGia = req.body.thiGia;
   let  bienDo = req.body.bienDo;
   let  klgd = req.body.klgd;
   console.log(req,"dau vao")
   let data = await Scheme.create({maCp:maCp,thiGia:thiGia,bienDo:bienDo,klgd:klgd});
   console.log(data,"ádfghjuiop");
   res.send({data})
  } catch (error) {
    res.send(error);
  }
};
exports.delete = async (req, res) => {
  try {
    let id = req.params.id;
    let data = await Scheme.findByIdAndDelete(id);
    res.send({ data });
  } catch (error) {
    res.send(error);
  }
};
exports.update = async (req, res) => {
  try {
    let id = req.params.id;
    let name = req.body.name;
    let limit = parseInt(req.query.limit);
    let textSearch = req.query.textSearch;
    let number;

    let data = await Scheme.findByIdAndUpdate(
      id,
      { name: name },
      { new: true }
    );
    let dataAll = await Scheme.find({
      name: { $regex: textSearch, $options: "i" },
    });
    for (let i = 0; i < dataAll.length; i++) {
      if (dataAll[i]._id.equals(id)) {
        number = i;
      }
    }
    let activePage = Math.ceil((number + 1) / limit);
    console.log(activePage, "da vao day");
    res.send({ data, activePage });
  } catch (error) {
    res.send(error);
  }
};
exports.search = async (req, res) => {
  try {
    let textSearch = req.query.textSearch;
    let limit = parseInt(req.query.limit);
    let activePage = parseInt(req.query.activePage);
    let skip = (activePage - 1) * limit;
    let totalRecords = await Scheme.countDocuments({
      name: { $regex: textSearch, $options: "i" },
    });
    let totalPage = Math.ceil(totalRecords / limit);
    let data = await Scheme.find({
      name: { $regex: textSearch, $options: "i" },
    })
      .skip(skip)
      .limit(limit);
    if (totalPage === 0) totalPage = 1;
    res.send({ data, totalPage });
  } catch (error) {
    res.send(error);
  }
};
exports.pagination = async (req, res) => {
  try {
    let limit = parseInt(req.query.limit);
    let activePage = parseInt(req.query.activePage);
    let skip = (activePage - 1) * limit;
    let totalRecords = await Scheme.countDocuments();
    let totalPage = Math.ceil(totalRecords / limit);
    if (totalPage === 0) totalPage = 1;
    let data = await Scheme.find().skip(skip).limit(limit);
    res.send({ data, totalPage });
  } catch (error) {
    res.send(error);
  }
};
const http = require("http");

const options = {
  // hostname: 'example.com',
  port: 8083,
  path: "/user",
  method: "GET",
  headers: {
    "Content-Type": "application/json",
  },
};
exports.callApiJava =  (req, res) => {
  console.log("vao day choi");
  try {
    const request = http.request(options, (response) => {
      console.log("a cthe chaajaj");
      response.setEncoding("utf8");
      response.on("data", (data) => {
         //console.log(data,"data ve")
         listData = JSON.parse(data);
         //console.log(data,"data ve");
      });
      response.on("end", () => {
        // console.log('Response data:',listData);
      });
   
    });
    request.on("error", (e) => {
      console.error(`Error: ${e.message}`);
    });
    request.end();
    // res.send({ listData });
    res.send({ listData });
  } catch (error) {
    res.send(error);
  }
};
