const todoList=require('../controller/index')
module.exports=function(app){
    app.route('/student')
        .get(todoList.get)
        .post(todoList.add)
    app.route('/:id')
        .put(todoList.update)
        .delete(todoList.delete)
    app.route("/search")    
        .get(todoList.search)
    app.route("/pagination")    
        .get(todoList.pagination)
    app.route("/getAll")
        .get(todoList.callApiJava)
    app.route("/test")
        .post(todoList.addCategory)
}