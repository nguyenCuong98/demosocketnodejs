var express = require("express"),
  app = express(),
  port = process.env.PORT || 3001,
  mongoose = require("mongoose"),
  bodyParser = require("body-parser");
cors = require("cors");
mongoose.Promise = global.Promise;
mongoose
  .connect("mongodb://localhost:27017/lamg", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Connected !!!");
  })
  .catch((err) => {
    console.log(err);
  });
const server = require("http").createServer(app);
const io = require("socket.io")(server, { cors: { origin: "*" } });
const http = require("http");

const options = {
  // hostname: 'example.com',
  port: 8083,
  path: "/category",
  method: "GET",
  headers: {
    "Content-Type": "application/json",
  },
};


io.on("connection", (socket) => {
  console.log(socket.id + "" + "đã connect tới service");
  var listData;
  socket.on("getData", () => {
    const request = http.request(options, (response) => {
      response.setEncoding("utf8");
      response.on("data", (data) => {
         listData = JSON.parse(data);
         socket.emit("data", listData);
      });
      response.on("end", () => {
        //console.log('Response data:',listData);
      });
   
    });
    request.end();
    request.on("error", (e) => {
      console.error(`Error: ${e.message}`);
    });
  });
});

app.use(cors({}));
app.use(bodyParser.json());

var routes = require("./api/route");

routes(app);

app.use(function (req, res) {
  res.status(404).send({ url: req.originalUrl + " not found" });
});

server.listen(3001, () => {
  console.log("server running ....");
});
console.log("Server started on: " + port);
